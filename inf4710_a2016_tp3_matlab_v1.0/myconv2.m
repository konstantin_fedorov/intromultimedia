function [ result ] = myconv2( array, kernel )

arraySize = size(array);
kernelSize = size(kernel);
padding = floor(kernelSize/2);

result = zeros(arraySize);

array = padarray(array, padding, 'replicate', 'both')

for i = 1:kernelSize(1)
    for j = 1:kernelSize(2)
       result = result + array(i :end - kernelSize(1) + i, ...
                               j :end - kernelSize(2) + j) * kernel(i,j);
    end
end

end

