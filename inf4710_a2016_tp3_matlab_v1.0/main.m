% INF4710 A2016 TP3 v1.0

close all;
clear;
clc;

% Affichage d'information de debuggage
global isDebug;
isDebug = 1;

%% Parametres ajustables
% Nombre de regroupements des histogrammes
nbBucketsHisto = 50; 
% Saut de trames lorsque on ne detecte pas une transition
baseFrameInterval = 60; 
% Difference dans les histogrammes entre deux trames separes de 
% #baseFrameInterval
detectionDelta = 1; 
% Taux maximal de difference de l'histogramme par rapport � la moyenne
% pour la detection du debut et de la fin de transitions
detectionStartDelta = 2;
% Taux de variation maximal de la difference des histogrammes pour la 
% detection de la fin des transitions progressives
detectionEndProgressive = 0.009; 


%% Programme

% Premiere trame
precedentFrame = 1;
frameInterval = baseFrameInterval;
lastDiff = -1;
vrobj = VideoReader('data/TP3_video.avi');
image = read(vrobj,precedentFrame);
nFrames = vrobj.NumberOfFrames;
histograms = ones(3, nbBucketsHisto, nFrames) * -1;
histograms(:,:,precedentFrame) = histo(image, nbBucketsHisto);

transitionStarted = 0;
t = precedentFrame + baseFrameInterval;
isDetecting = 0;
nbFrameDetecting = 0;
runningMean = -1;

transitions = [];
transitionCount = 1;

while t <= nFrames
    image = read(vrobj,t);
    if isDebug
        imshow(image);
    end
    currentHisto = histo(image, nbBucketsHisto);
    histograms(:,:,t) = currentHisto;
    curDiff = sum(sum(abs(histograms(:,:,precedentFrame)-currentHisto)));
    diffChange = 0;
    debugPrint('%d difference %f',t, curDiff);
    if isDetecting == 0
        if lastDiff ~= -1
            diffChange = abs(curDiff-lastDiff);
        end
        if curDiff > detectionDelta
            %DETECT EXACTLY THE MOMENT OF CHANGE 
            t = precedentFrame + 1;
            isDetecting = 1;
            nbFrameDetecting = 0;
            runningMean = -1;
        else
            lastDiff = curDiff;
            precedentFrame = t;
            increment = max(min(frameInterval, nFrames - t ),1);
            t = t + increment;
            debugPrint(', taux %f, int %d', diffChange, frameInterval);
        end
    else
        if runningMean == -1
            runningMean = curDiff;
        else
            diffChange = abs(curDiff-lastDiff);
            nbFrameDetecting = nbFrameDetecting + 1;
            runningMean = runningMean * 0.4 + 0.6 * lastDiff;
            debugPrint(', taux %f, moyenne %f',diffChange, runningMean);
            if ~transitionStarted && nbFrameDetecting > baseFrameInterval
                fprintf(1,' Faux positif! ');
                isDetecting = false;
            elseif ~transitionStarted && curDiff > detectionStartDelta * runningMean
                debugPrint(' Debut TRANSITION! ');
                transitionStarted = 1;
                runningMean = curDiff;
                transitions(transitionCount,1) = t;
            elseif transitionStarted && (curDiff < (1 / detectionStartDelta) * runningMean || diffChange < detectionEndProgressive)
                debugPrint(' Fin TRANSITION! ');
                transitionStarted = 0;
                isDetecting = false;
                transitions(transitionCount,2) = t;
                transitionCount = transitionCount + 1;
            end
        end
        lastDiff = curDiff;
        precedentFrame = t;
        t = t+1;
    end
    
    debugPrint('\n');   
end

for i = 1:transitionCount-1
    if(transitions(i,2) == transitions(i,1) + 1)
        fprintf(1,'Transition instantann�e - trame: %d\n',transitions(i,1));
    else
        fprintf(1,'Transition progressive - trame de debut: %d, trame de fin: %d\n',transitions(i,1),transitions(i,2));
    end
end