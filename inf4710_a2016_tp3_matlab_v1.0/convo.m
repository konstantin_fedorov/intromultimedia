function [ result ] = convo( image, kernel )
    assert(numel(image)>0 && ndims(image)==3 && numel(kernel)>0 && ndims(kernel)==2);
    assert(size(image,1)>size(kernel,1) && size(image,2)>size(kernel,2));
    
    R = image(:,:,1);
    G = image(:,:,2);
    B = image(:,:,3);
    
    Rconv = myconv2(R, kernel);
    Gconv = myconv2(G, kernel);
    Bconv = myconv2(B, kernel);
    
    result = cat(3, Rconv, Gconv, Bconv);
    
end