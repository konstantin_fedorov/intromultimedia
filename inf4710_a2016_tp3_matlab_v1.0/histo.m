function [ hist ] = histo( image, N )
    assert(numel(image)>0 && ndims(image)==3 && numel(N)==1 && N>1 && N<=256);
    hist = zeros(3,N);
    
    for i = 1:size(image,1)
        for j = 1:size(image,2)
            values = ceil(double(image(i,j,:)+1)*N/256);
            for c = 1:3
                hist(c,values(c)) = hist(c,values(c)) + 1;
            end
        end
    end
    
    hist = hist / (size(image,1) * size(image,2));
    
end