function [] = debugPrint( varargin )
    global isDebug;
    if isDebug
        fprintf(1, varargin{:});
    end
end

