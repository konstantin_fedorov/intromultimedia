% zigzag: returns a (NxN)-element 1D array created by zig-zagging through a NxN block
function [array] = zigzag(block)
    assert(numel(block)>0 && size(block,1)==size(block,2));
    array = zeros(size(block,1)*size(block,2),1,class(block));
    
    block_size = size(block,1);
    idx = 1;
    for i=0:block_size*2-1
        j = max(0,i-block_size+1);
        while (j<=i &&  j<block_size)
            if mod(i,2)==0
                array(idx) = block(j*(block_size-1)+i+1);
            else
                array(idx) = block((i-j)*block_size+j+1);
            end
            j = j + 1;
            idx = idx + 1;
        end
    end
    
end
