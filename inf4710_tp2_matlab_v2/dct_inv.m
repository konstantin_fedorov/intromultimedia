% dct_inv: computes the inverse discrete cosinus tranform of a matrix
function [block] = dct_inv(block_dct)
    assert(numel(block_dct)>0 && size(block_dct,1)==size(block_dct,2) && mod(size(block_dct,1),2)==0);
    block = zeros(size(block_dct),'uint8');

    n = size(block,1);
    block_dct = double(block_dct);

    persistent dct_mat;
    global c_vect;
    if isempty(c_vect)
        sqrt1 = sqrt(1/n);
        sqrt2 = sqrt(2/n);
        for u = 1:n
            for v = 1:n
                ret = 1;
                if u == 1
                    ret = ret * sqrt1;
                else
                    ret = ret * sqrt2;
                end
                if v == 1
                    ret = ret * sqrt1;
                else
                    ret = ret * sqrt2;
                end
                c_vect(u,v) = ret;
            end
        end
    end
    
    dct_mat_x = zeros(n,n,n);
    dct_mat_y = zeros(n,n,n);
    for x = 1:n
        for y = 1:n
             if x > size(dct_mat,3) || y > size(dct_mat,4) || n > size(dct_mat,1) || dct_mat(1,1,x,y) == 0
                if dct_mat_x(1,1,x) == 0
                    u = [1:n].';
                    dct_mat_x(:,:,x) = repmat(cos((pi*(2*(x-1)+1)*(u-1))/(2*n)), [1 n]);
                end
                if dct_mat_y(1,1,y) == 0
                    v = 1:n;
                    dct_mat_y(:,:,y) = repmat(cos((pi*(2*(y-1)+1)*(v-1))/(2*n)), [n 1]);
                end
                dct_mat(1:n,1:n,x,y) = dct_mat_x(:,:,x) .* dct_mat_y(:,:,y);
             end
            block(x,y) = sum(sum( c_vect .* block_dct .* dct_mat(1:n,1:n,x,y)));
        end
    end
    
end