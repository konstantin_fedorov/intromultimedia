% INF4710 A2016 TP2 v1

close all;
clear;
clc;

USE_SUBSAMPLING = 1;
USE_QUANT_QUALITY = 25;

test_image_paths = { ...
    'data/airplane.png', ...
    'data/baboon.png', ...
    'data/cameraman.tif', ...
    'data/lena.png', ...
    'data/logo.tif', ...
    'data/logo_noise.tif', ...
    'data/peppers.png', ...
};

for t=1:numel(test_image_paths)
    input = imread(test_image_paths{t});
    assert(numel(input)>0);
    if(size(input,3)==1)
        input = cat(3,input,input,input);
    elseif(size(input,3)==4)
        input = input(:,:,1:3);
    end
    
    % COMPRESSION
    [Y,Cb,Cr] = conv_rgb2ycbcr(input,USE_SUBSAMPLING);
    blocks_y = decoup(Y);
    blocks_cb = decoup(Cb);
    blocks_cr = decoup(Cr);
    blocks = cat(3,blocks_y,blocks_cb,blocks_cr);
    blocks_dct = zeros(size(blocks));
    for b=1:size(blocks,3)
        blocks_dct(:,:,b) = dct(blocks(:,:,b));
    end
    blocks_quantif = zeros(size(blocks_dct),'int16');
    for b=1:size(blocks_dct,3)
        blocks_quantif(:,:,b) = quantif(blocks_dct(:,:,b),USE_QUANT_QUALITY);
    end
    blocks_vectors = zeros(size(blocks_quantif,1)*size(blocks_quantif,2),size(blocks_quantif,3),'int16');
    for b=1:size(blocks_quantif,3)
        blocks_vectors(:,b) = zigzag(blocks_quantif(:,:,b));
    end
    code = huff(blocks_vectors);

    % @@@@ TODO: check compression rate here...
    ratio = 1- (numel(code.string)/8 / numel(input));
    fprintf(1,'#%d Compression ration: %.2f\n', t, ratio);
    % DECOMPRESSION
%     blocks_vectors_decompr = blocks_vectors;
    blocks_vectors_decompr = huff_inv(code,8*8);
    blocks_quantif_decompr = zeros([sqrt(size(blocks_vectors,1)),sqrt(size(blocks_vectors,1)),size(blocks_quantif,3)],'int16');
    for b=1:size(blocks_vectors_decompr,2)
        blocks_quantif_decompr(:,:,b) = zigzag_inv(blocks_vectors_decompr(:,b));
    end
    blocks_dct_decompr = zeros(size(blocks_quantif_decompr));
    for b=1:size(blocks_quantif_decompr,3)
        blocks_dct_decompr(:,:,b) = quantif_inv(blocks_quantif_decompr(:,:,b),USE_QUANT_QUALITY);
    end
    blocks_decompr = zeros(size(blocks_dct_decompr),'uint8');
    for b=1:size(blocks_decompr,3)
        blocks_decompr(:,:,b) = dct_inv(blocks_dct_decompr(:,:,b));
    end
    
    Y_decompr = decoup_inv(blocks_decompr(:,:,1:size(blocks_y,3)),size(Y));
    Cb_decompr = decoup_inv(blocks_decompr(:,:,size(blocks_y,3)+1:size(blocks_y,3)+size(blocks_cb,3)),size(Cb));
    Cr_decompr = decoup_inv(blocks_decompr(:,:,size(blocks_y,3)+1+size(blocks_cb,3):end),size(Cr));
    input_decompr = conv_ycbcr2rgb(Y_decompr,Cb_decompr,Cr_decompr,USE_SUBSAMPLING);

    figure;
    imshow(cat(2,input,input_decompr,imabsdiff(input,input_decompr)));
end
fprintf('all done\n');
