% decoup_inv: reconstructs a 2D image (i.e. always single channel) from a block array
function [img] = decoup_inv(blocks,img_size)
    assert(numel(blocks)>0 && size(blocks,1)==size(blocks,2));
    block_size = size(blocks,1);
    assert(numel(img_size)==2 && img_size(1)*img_size(2)>0 && mod(img_size(1),block_size)==0 && mod(img_size(2),block_size)==0);
    img = zeros(img_size,'uint8');

    % @@@@ TODO
    nbBlocksVert = size(img,2) / block_size;
    for i = 1:block_size:img_size(1)
        for j = 1:block_size:img_size(2)
            img(i:i+block_size-1, j:j+block_size-1) = blocks(:,:, ((i-1)/block_size*nbBlocksVert) + (j-1)/block_size + 1);
        end
    end

end