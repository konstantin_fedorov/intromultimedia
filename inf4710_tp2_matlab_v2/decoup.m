% decoup: reformats a 2D image (i.e. always single channel) to a block array
function [blocks] = decoup(img,block_size)
    if ~exist('block_size','var')
        block_size = 8;
    end
    assert(numel(img)>0 && mod(size(img,1),block_size)==0 && mod(size(img,2),block_size)==0);
    
    nbBlocks = (size(img,1)/block_size) * (size(img,2)/block_size);
    nbBlocksVert = size(img,2) / block_size;
    blocks = zeros([block_size block_size nbBlocks],'uint8');
    for i = 1:block_size:size(img,1)
        for j = 1:block_size:size(img,2)
            blocks(:,:, ((i-1)/block_size*nbBlocksVert) + (j-1)/block_size + 1) = img(i:i+block_size-1, j:j+block_size-1);
        end
    end
end