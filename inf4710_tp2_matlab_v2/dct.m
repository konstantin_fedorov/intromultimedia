% dct: computes the discrete cosinus tranform of a matrix
function [block_dct] = dct(block)
    assert(numel(block)>0 && size(block,1)==size(block,2) && mod(size(block,1),2)==0);
    block_dct = zeros(size(block));
    
    n = size(block,1);
    block = double(block);

    persistent dct_mat;
    global c_vect;
    if isempty(c_vect)
        sqrt1 = sqrt(1/n);
        sqrt2 = sqrt(2/n);
        for u = 1:n
            for v = 1:n
                ret = 1;
                if u == 1
                    ret = ret * sqrt1;
                else
                    ret = ret * sqrt2;
                end
                if v == 1
                    ret = ret * sqrt1;
                else
                    ret = ret * sqrt2;
                end
                c_vect(u,v) = ret;
            end
        end
    end
    
    dct_mat_x = zeros(n,n,n);
    dct_mat_y = zeros(n,n,n);
    for u = 1:n
        for v = 1:n
            if u > size(dct_mat,3) || v > size(dct_mat,4) || n > size(dct_mat,1) || dct_mat(1,1,u,v) == 0
                if dct_mat_x(1,1,u) == 0
                    x = [1:n].';
                    dct_mat_x(:,:,u) = repmat(cos((pi*(2*(x-1)+1)*(u-1))/(2*n)), [1 n]);
                end
                if dct_mat_y(1,1,v) == 0
                    y = 1:n;
                    dct_mat_y(:,:,v) = repmat(cos((pi*(2*(y-1)+1)*(v-1))/(2*n)), [n 1]);
                end
                mat_x = dct_mat_x(:,:,u);
                mat_y = dct_mat_y(:,:,v);
                dct_mat(1:n,1:n,u,v) = mat_x .* mat_y;
            end
            
            block_dct(u,v) = c_vect(u,v) * sum(sum(block .* dct_mat(1:n,1:n,u,v)));
            
        end
    end
end

