function signal = huff_inv(code,chunk_size)
    if ~exist('chunk_size','var')
        chunk_size = 64;
    end
    assert(numel(code.string)>1 && numel(code.map)>0 && numel(size(code.map))==2);
    string = code.string;
    symbols_list = cell2mat(code.map(:,1));
    symbols_strings = code.map(:,2);
    signal = zeros(numel(string),1,'int16');
    trie = buildBinaryTrie(symbols_strings);
    i = 1;
    signalIdx = 1;
    length = numel(string);
    while i <= length
       curNode = trie;
       curSymbol = curNode{3};
       while isempty(curSymbol)
           curNode = curNode{string(i)+1};
           curSymbol = curNode{3};
           i = i + 1;
       end
       signal(signalIdx) = symbols_list(curSymbol);
       signalIdx = signalIdx + 1;
    end
    signal = signal(1:signalIdx-1);
    
    signal = reshape(signal,chunk_size,[]);
    assert(size(signal,1)==chunk_size);
end

function trie = buildBinaryTrie(strings)
    trie = {}; 
    for istring = 1:numel(strings)
       trie = addStringToTrie(strings{istring},trie, istring);
    end
end

function trie = addStringToTrie(string, trie, idx)
    if isempty(trie)
        trie = {{} {} {}}; % trie{1} = branch 0, trie{2} = branch 1, trie{3} = string index
    end
    if numel(string) == 0
        trie{3} = idx;
    else
        trie{string(1)+1} = addStringToTrie(string(2:end),trie{string(1)+1}, idx);
    end
end