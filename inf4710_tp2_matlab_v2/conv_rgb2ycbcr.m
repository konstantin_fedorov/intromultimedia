% conv_rgb2ycbcr: converts an 8-bit-depth RGB image to Y'CbCr format using optional 4:2:0 subsampling
function [Y, Cb, Cr] = conv_rgb2ycbcr(RGB, subsample)
    assert(numel(RGB)>0 && size(RGB,3)==3);
    
    RGB = double(RGB);
    Y = double(0.299 * RGB(:,:,1) + 0.587 * RGB(:,:,2) + 0.114 * RGB(:,:,3));
    if subsample
        Cb = uint8(128 * ones(size(RGB,1)/2,size(RGB,2)/2) + 0.564 ...
            * (RGB(1:2:end,1:2:end,3) - Y(1:2:end,1:2:end)));
        Cr = uint8(128 * ones(size(RGB,1)/2,size(RGB,2)/2) + 0.713 ...
            * (RGB(1:2:end,1:2:end,1) - Y(1:2:end,1:2:end)));
    else
        Cb = uint8(128 * ones(size(RGB,1),size(RGB,2)) + 0.564 ...
                * (RGB(:,:,3) - Y));
        Cr = uint8(128 * ones(size(RGB,1),size(RGB,2)) + 0.713 ...
                * (RGB(:,:,1) - Y));
    end
    Y = uint8(Y);
end