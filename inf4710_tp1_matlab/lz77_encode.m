% lz77_encode: encodes a signal (1D uint8 vector) using LZ77 algo
function [code] = lz77_encode(signal,N,n1)
    %    'code' is expected to be an array of structures, each with three fields:
    %            - 'idx' (uint8) the in-dictionary index offset for the first match
    %            - 'length' (uint8) the total consecutive match count
    %            - 'next' (uint8) the next unmatched symbol
    %       e.g. initialize with:   code(idx) = struct('idx',uint8(..),'length',uint8(..),'next',uint8(..));
    assert(N>0 && n1>0 && N>n1 && n1<255 && ndims(signal)==2 && numel(signal)>1 && isa(signal(1),'uint8'));
    
    % Preallocate the code, worst case is all 0,0,symbol 
    % so max length = length of signal
    code = repmat(struct('idx',-1,'length',-1,'next',-1), 1, length(signal) ); 
    signal = [zeros(1,n1-1), signal]; 
    codeIdx = 1;
    idx = n1;
    % iterate on signal, except the last symbol
    while idx < length(signal)
        next = signal(idx);
        itemLength = 0;
        idxToSave = 0;
        dictIdx = idx-1;
        % loop back in dictionary until we find a match
        while dictIdx > idx-n1 && itemLength <= 0
            % if matching, find the matching length
            while(idx + itemLength < length(signal) - 1 ...
                    && dictIdx + itemLength < idx + N - n1 ...
                    && signal(dictIdx + itemLength) == signal(idx + itemLength))
                itemLength = itemLength + 1;
            end
            if(itemLength > 0)
                idxToSave = idx - dictIdx;
                next = signal(idx+itemLength);
            end
            dictIdx  = dictIdx - 1;
        end
        %Save the code and increment the indexes
        code(codeIdx) = struct('idx',uint8(idxToSave),'length',uint8(itemLength),'next',uint8(next));
        idx = idx + itemLength + 1;
        codeIdx = codeIdx + 1;
    end
    % add the last symbol
    code(codeIdx) = struct('idx',uint8(0),'length',uint8(0),'next',uint8(signal(end)));
    code = code(1:codeIdx);
end

