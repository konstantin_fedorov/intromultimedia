% reformat_image: reformats a 1D uint8 vector to a 2D or 3D image (i.e. may be multi-channel)
function [image] = reformat_image(signal,img_size)
    assert(numel(img_size)>=2 && ndims(signal)==2 && numel(signal)>1 && isa(signal(1),'uint8'));
    if(numel(img_size)==3)
        image = permute(reshape(signal,[img_size(2) img_size(1) img_size(3)]),[2 1 3]);
    else
        image = reshape(signal,[img_size(2) img_size(1)])';
    end
end

