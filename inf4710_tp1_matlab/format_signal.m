% format_signal: reformats a 2D or 3D image (i.e. may be multi-channel) to a 1D uint8 vector
function [signal] = format_signal(image)
    assert(ndims(image)>=2 && numel(image)>1 && isa(image(1),'uint8'));
    % @@@@ TODO (hint: matlab stores data in col-major format!)
    imageSize = size(image);
    totalSize = imageSize(1) * imageSize(2);
    signal = reshape(image(:,:,1)',1,totalSize);
    if(ndims(image) == 3)
        signal = [signal reshape(image(:,:,2)',1,totalSize)];
        signal = [signal reshape(image(:,:,3)',1,totalSize)];
    end
end

