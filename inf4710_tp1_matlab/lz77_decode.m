% lz77_decode: reads an LZ77 struct vector, and decodes it into its original signal
function [signal] = lz77_decode(code,N,n1)
    %    'code' is expected to be an array of structures, each with three fields:
    %            - 'idx' (uint8) the in-dictionary index offset for the first match
    %            - 'length' (uint8) the total consecutive match count
    %            - 'next' (uint8) the next unmatched symbol
    assert(N>0 && n1>0 && N>n1 && n1<255 && ndims(code)==2 && numel(code)>1 && isstruct(code(1)));
    
    % Preallocate, might be unsufficient so we will have to increase the
    % size later
    % IMPROVEMENT : Get the size from the sum of code.length + length(code)
    signal =  uint8(zeros(1, length(code)));
    signalIdx = n1 + 1; 
    idx = 1;
    % Loop on code
    while idx <= length(code)
        curCode = code(idx);
        if signalIdx + N > length(signal)
            % allocate more space: double the size of the array
            signal = [signal uint8(zeros(1, length(signal)))];
        end
        if curCode.length == 0
            % Add symbol if 0 0 symbol
            signal(signalIdx) = uint8(curCode.next);
            signalIdx = signalIdx + 1;
        else
            % Add sequence
            curCodeIndex =  uint32(curCode.idx); 
            curCodeLength = uint32(curCode.length);
            % offset = length of sequence repeating beyond back window
            offset = max(curCodeLength - curCodeIndex, 0);
            dictionaryIdx = signalIdx - curCodeIndex;
            % toCopy = sequence in dictionary that will be repeated for
            % offset
            toCopy = signal(dictionaryIdx  : dictionaryIdx - offset  + (curCodeLength - 1));
            repeated = repmat(toCopy,1,ceil(double(offset)/length(toCopy)));
            % Add the sequence to the signal
            toAdd = uint8([toCopy repeated(1:offset) curCode.next]);
            signal(signalIdx : signalIdx+length(toAdd)-1) = toAdd;
            signalIdx = signalIdx + length(toAdd);
        end
        idx = idx + 1;
    end
    signal = signal(n1+1:signalIdx-1);
end

